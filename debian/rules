#!/usr/bin/make -f
# Sample debian/rules that uses debhelper.
# GNU copyright 1997 to 1999 by Joey Hess.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# These are from the autotools-dev package documentation.
# /usr/share/doc/autotools-dev/README.Debian.gz
# from the section titled "Calling GNU configure properly"
export DEB_HOST_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
export DEB_BUILD_GNU_TYPE ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)

# FOR AUTOCONF 2.52 AND NEWER ONLY
ifeq ($(DEB_BUILD_GNU_TYPE), $(DEB_HOST_GNU_TYPE))
  confflags += --build $(DEB_HOST_GNU_TYPE)
else
  confflags += --build $(DEB_BUILD_GNU_TYPE) --host $(DEB_HOST_GNU_TYPE)
endif

# CXXFLAGS to use. Allowing it to be overriden.
DEFAULT_CXXFLAGS = -Wall -g
ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
   DEFAULT_CXXFLAGS += -O0
else
   DEFAULT_CXXFLAGS += -O2
endif
DEB_CXXFLAGS ?= $(DEFAULT_CXXFLAGS)

# Include a OGRE_CONFIG_OPTIONS variable that has the default configure options
# used to build this package. This variable can be overridden.
OGRE_CONFIG_OPTIONS ?=  $(confflags) \
		--prefix=/usr \
		--host=$(DEB_HOST_GNU_TYPE) \
		--build=$(DEB_BUILD_GNU_TYPE) \
		--mandir=\$${prefix}/share/man \
		--infodir=\$${prefix}/share/info \
		--with-gui=Xt \
		--enable-cg \
		--disable-ogre-demos \
		CXXFLAGS="$(DEB_CXXFLAGS)"

patch: patch-stamp
patch-stamp:
	dh_testdir
	QUILT_PATCHES=debian/patches quilt push -a || test $$? = 2
	touch patch-stamp

configure: patch configure-stamp
configure-stamp:
	dh_testdir
	# Modified bootstrapping from upstream, declares everything obsolete.
	libtoolize --force --copy
	aclocal -I Scripts/m4 $(ACLOCAL_FLAGS)
	autoheader -f
	automake --include-deps --add-missing -f --foreign --copy
	autoconf -f
	./configure $(OGRE_CONFIG_OPTIONS)
	touch configure-stamp

build: configure build-stamp
build-stamp:
	dh_testdir
	$(MAKE)
	touch build-stamp

clean: clean-stamp unpatch
clean-stamp:
	dh_testdir
	dh_testroot
	[ ! -f Makefile ] || $(MAKE) clean distclean
	rm -f config.log
	rm -f configure-stamp build-stamp
	dh_clean

unpatch:
	dh_testdir
	QUILT_PATCHES=debian/patches quilt pop -a -R || test $$? = 2
	rm -rf .pc/ patch-stamp

install: build
	dh_testdir 
	dh_testroot
	dh_prep
	install -d debian/tmp
	$(MAKE) install DESTDIR=$(CURDIR)/debian/tmp
	install -d debian/tmp/etc/OGRE
	install -m 644 debian/plugins.cfg.cgprogrammanager \
		debian/tmp/etc/OGRE/plugins.cfg.cgprogrammanager

# Build architecture-independent files here.
binary-indep:
# Nothing to do for binary-indep

# Build architecture-dependent files here.
binary-arch: build install
	dh_testdir
	dh_testroot
	dh_install --sourcedir=debian/tmp
	dh_installchangelogs
	dh_installdocs
	dh_strip --dbg-package=ogre-plugins-cgprogrammanager-dbg
	dh_compress
	dh_fixperms
	dh_makeshlibs
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch
.PHONY: configure build clean binary-indep binary-arch binary install
